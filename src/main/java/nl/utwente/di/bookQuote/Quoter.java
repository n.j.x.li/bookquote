package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
     public double getBookPrice(String isbn){
         HashMap<String, Double> BookPrices = new HashMap<String, Double>();
         BookPrices.put("1", 10.0);
         BookPrices.put("2", 45.0);
         BookPrices.put("3", 20.0);
         BookPrices.put("4", 35.0);
         BookPrices.put("5", 50.0);
         return BookPrices.get(isbn);
     }

}
